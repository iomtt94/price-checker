require('dotenv').config();
const nightmare = require('nightmare')();
const process = require('process');
const args = process.argv.slice(2);
const url = args[1];
const minPrice = Number(args[0]);

const SGMail = require('@sendgrid/mail');
SGMail.setApiKey(process.env.SENDGRID_API_KEY);



checkPrice();

async function checkPrice() {
    try {
        const priceString = await nightmare
                                    .goto(url)
                                    .wait(".b-price")
                                    .evaluate(() => document.querySelector(".b-pmi-col-left .b-price").innerText)
                                    .end();

        const priceNumber = parseFloat(priceString.replace(/\D/g,''));

        if (priceNumber < minPrice) {
            console.log(`it's cheaper than you requested for ${ minPrice - priceNumber }₴ hryvnias`);
            sendEmail(
                'price is low',
                `it's cheaper than you requested for ${ minPrice - priceNumber }₴ hryvnias`
            );

        } else {
            console.log(`it's more expensive than you requested for ${ priceNumber - minPrice }₴ hryvnias`);
        }
    } catch(error) {
        sendEmail(
            'Price checker error, something wrong has happened',
            error.message
        );

        throw error;
    }
}

function sendEmail(subject, body) {
    const email = {
        to: 'iomtt94@gmail.com',
        from: 'telemart-price-checker@example.com',
        subject: subject,
        text: body,
        html: body
    }

    return SGMail.send(email);
}



// example of request - ||   node parser.js 7000 https://telemart.ua/products/amd-ryzen-5-3600x-3644ghz-32mb-sam4-box-100-100000022box  ||
